package com.example

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class MainTest {
    @Test
    fun testOk() {
        assertEquals("OK", testOkString())
    }

    @Test
    fun testNotOk() {
        assertEquals("NOT OK", testNoOkString())
    }
}