import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    testImplementation("org.junit.jupiter:junit-jupiter:5.4.0")
}

plugins {
    kotlin("jvm") version "1.3.21"
}

sourceSets.main {
    withConvention(KotlinSourceSet::class) {
        kotlin.srcDirs("src")
    }
}
sourceSets.test {
    withConvention(KotlinSourceSet::class)  {
        kotlin.srcDirs("test")
    }
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}